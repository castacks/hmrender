#ifndef BRESENHAM_H_90FY87IP
#define BRESENHAM_H_90FY87IP

namespace hmrender
{

class BresenhamIterator2 {
public:
  BresenhamIterator2() :
      x0_(0),
      y0_(0),
      x1_(0),
      y1_(0),
      x_(0),
      y_(0)
  { }

  BresenhamIterator2(int x0, int y0,
                     int x1, int y1) :
      x0_(x0),
      y0_(y0),
      x1_(x1),
      y1_(y1),
      x_(x0),
      y_(y0)
  {
    this->init();
  }

  void reset(int x0, int y0,
             int x1, int y1) {
    x0_ = x0;
    y0_ = y0;
    x1_ = x1;
    y1_ = y1;
    x_ = x0;
    y_ = y0;
    this->init();
  }

  void init() {
    x_ = x0_;
    y_ = y0_;

    dx_ = x1_ - x0_;
    dy_ = y1_ - y0_;

    //X
    if (dx_>0) {
      sx_ = 1;
    } else if (dx_<0) {
      sx_ = -1;
      dx_ = -dx_;
    } else {
      sx_ = 0;
    }

    //Y
    if (dy_>0) {
      sy_ = 1;
    } else if (dy_<0) {
      sy_ = -1;
      dy_ = -dy_;
    } else {
      sy_ = 0;
    }

    ax_ = 2*dx_;
    ay_ = 2*dy_;

    if (dy_ <= dx_) {
      decy_ = ay_-dx_;
    } else {
      decx_ = ax_-dy_;
    }

    done_ = false;
  }

  void step() {
    if (dy_ <= dx_){
      if (decy_ >= 0) {
        decy_ -= ax_;
        y_ += sy_;
      }
      x_ += sx_;
      decy_ += ay_;
    } else {
      if (decx_ >= 0) {
        decx_ -= ay_;
        x_ += sx_;
      }
      y_ += sy_;
      decx_ += ax_;
    }
    // note we can continue past 'done' point.
    if (x_ == x1_ && y_ == y1_) {
      done_ = true;
    }
  }

  int i() const { return x_; }

  int j() const { return y_; }

  bool done() const { return done_; }

  ~BresenhamIterator2() { }
  BresenhamIterator2(const BresenhamIterator2& other) = delete;
  BresenhamIterator2& operator=(const BresenhamIterator2& other) = delete;

private:
  int x0_, y0_;
  int x1_, y1_;
  int x_, y_;
  int sx_ = 0, sy_ = 0;
  int ax_ = 0, ay_ = 0;
  int dx_ = 0, dy_ = 0;
  int decx_ = 0, decy_ = 0;
  bool done_ = false;
};


} /* hmrender */

#endif /* end of include guard: BRESENHAM_H_90FY87IP */
