/**
 * @author  Daniel Maturana
 * @year    2018
 *
 * @attention Copyright (c) 2018
 * @attention Carnegie Mellon University
 * @attention All rights reserved.
 */

#ifndef _HMRENDER_HMRENDER_H_
#define _HMRENDER_HMRENDER_H_

#include <cmath>

#include <cv_bridge/cv_bridge.h>
#include <pcl/common/transforms.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>
#include <ros/console.h>
#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include "tilegrid/tilegrid.h"
#include "roscpp_utils/caminfo.h"
#include "roscpp_utils/ros_wrapper.h"

namespace hmrender {

namespace sm = sensor_msgs;

class HeightReprojection {
public:
  HeightReprojection(RosWrapper::Ptr ros_wrapper) :
      ros_wrapper_(ros_wrapper) {
    ros_wrapper_->advertise<sm::Image>("reproj_depth", 10);
    ros_wrapper_->advertise<sm::Image>("reproj_disp", 10);
    ros_wrapper_->advertise<PCXYZ>("reproj_depth_pc", 10);
  }

  virtual ~HeightReprojection() { }

  HeightReprojection(const HeightReprojection& other) = delete;
  HeightReprojection& operator=(const HeightReprojection& other) = delete;

  void reproject_map(const Isometry3f& M,
                     const CamInfo& caminfo,
                     FloatGrid::Ptr grid,
                     cv::Mat& depth,
                     cv::Mat& ijkmap);

  void raysurf_map(const Isometry3f& M,
                   const CamInfo& caminfo,
                   HeightGrid::Ptr grid,
                   const ros::Time& ts,
                   cv::Mat& depth,
                   cv::Mat& ijkmap);

private:
  void publish_reproj_depth_pc(const std::string& frame_id,
                               const ros::Time& ts,
                               const cv::Mat& depth,
                               const CamInfo& caminfo);

  void publish_reproj_disp(const std::string& frame_id,
                           const ros::Time& ts,
                           const cv::Mat& depth);


private:
  RosWrapper::Ptr ros_wrapper_;
};

} /* hmrender */

#endif
