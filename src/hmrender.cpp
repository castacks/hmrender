#include "hmrender/hmrender.h"

#include <limits>

#include "tilegrid/ray.h"

#include "tictoc_profiler/profiler.hpp"
#include "hmrender/bresenham.h"

namespace hmrender
{

void HeightReprojection::publish_reproj_depth_pc(const std::string& frame_id,/*{{{*/
                                                 const ros::Time& ts,
                                                 const cv::Mat& depth,
                                                 const CamInfo& caminfo) {
  CA_AUTO_TICTOC(publish_reproj_depth_pc);

  if (!ros_wrapper_->has_subscribers("reproj_depth_pc")) { return; }
  // pub depth pc
  float ufx = caminfo.fx;
  float ufy = caminfo.fy;
  float ucx = caminfo.cx;
  float ucy = caminfo.cy;

  PCXYZ::Ptr reproj_depth_pc(new PCXYZ);
  for (int v=0; v < depth.rows; ++v) {
    for (int u=0; u < depth.cols; ++u) {
      float z = depth.at<float>(v, u);
      if (z == 0.) { continue; }
      float x = (static_cast<float>(u) - ucx)*z/ufx;
      float y = (static_cast<float>(v) - ucy)*z/ufy;
      pcl::PointXYZ p(x, y, z);
      reproj_depth_pc->push_back(p);
    }
  }
  reproj_depth_pc->width = reproj_depth_pc->size();
  reproj_depth_pc->height = 1;

  ros_wrapper_->publish_pcxyz("reproj_depth_pc", reproj_depth_pc, ts, frame_id);
}/*}}}*/

void HeightReprojection::publish_reproj_disp(const std::string& frame_id,/*{{{*/
                                             const ros::Time& ts,
                                             const cv::Mat& depth) {
  if (!ros_wrapper_->has_subscribers("reproj_disp")) { return; }
  CamInfo down_caminfo;
  if (!ros_wrapper_->get_caminfo(STEREO_CAMINFO_TOPIC, down_caminfo)) {
    return;
  }
  cv::Mat disp(depth.size(), depth.type());
  cv::MatIterator_<float> disp_itr = disp.begin<float>();
  cv::MatConstIterator_<float> depth_itr = depth.begin<float>();
  //ROS_INFO_STREAM("caminfo.Tx " << caminfo.Tx);
  for (; depth_itr != depth.end<float>(); ++depth_itr, ++disp_itr) {
    float depthval = *depth_itr;
    *disp_itr = (depthval == 0.) ? 0. : (-down_caminfo.Tx/depthval);
  }
  ros_wrapper_->publish_cv("reproj_disp", disp, "32FC1", ts, frame_id);
}/*}}}*/

void HeightReprojection::reproject_map(const Isometry3f& M,/*{{{*/
                                       const CamInfo& caminfo,
                                       FloatGrid::Ptr grid,
                                       cv::Mat& depth,
                                       cv::Mat& ijmap) {
  CA_AUTO_TICTOC(reproject_map);

  // render the grid onto depth (which also acts as z-buffer)
  // and ijmap (which maps for each pixel to an ijk voxel).
  // then semantics uses ijmap to project semantics to map.
  float ufx = caminfo.fx;
  float ufy = caminfo.fy;
  float ucx = caminfo.cx;
  float ucy = caminfo.cy;
  int uh = caminfo.width;
  int uw = caminfo.height;

  Isometry3f Minv(M.inverse());
  Vec3f origin_w, left_corner, right_corner;

  Vec2Ix origin_ij(grid->world_to_grid(origin_w.head<2>()));
  Vec2Ix left_corner_ij(grid->world_to_grid(left_corner.head<2>()));
  Vec2Ix right_corner_ij(grid->world_to_grid(right_corner.head<2>()));

  // clipping bounding box, easier to clip with than triangle
  // TODO clip directly with vdb? just for height layer?

  int i_min = std::min(std::min(origin_ij[0], left_corner_ij[0]), right_corner_ij[0]);
  int j_min = std::min(std::min(origin_ij[1], left_corner_ij[1]), right_corner_ij[1]);
  int i_max = std::max(std::max(origin_ij[0], left_corner_ij[0]), right_corner_ij[0]);
  int j_max = std::max(std::max(origin_ij[1], left_corner_ij[1]), right_corner_ij[1]);

  using Vec5f = Eigen::Matrix<float, 1, 5>;

  std::vector<Vec5f> uvd_ijs;

  for (FloatGrid::ValueOnIterator itr = grid->begin_value_on();
       itr.test();
       ++itr) {
    Vec2Ix ij(itr.get_grid_ix());
    // box clipping
    if ((ij[0] < i_min) ||
        (ij[1] < j_min) ||
        (ij[0] > i_max) ||
        (ij[1] > j_max)) {
      continue;
    }
    Vec2f p(grid->grid_to_world(ij));

    //HeightBinCell cell(itr.get_value());
    float height_w = itr.get_value();;
    Vec3f pe(p.x(), p.y(), height_w);

    // from world frame to (semantic) ueye frame
    Vec3f Mpe = Minv * pe;

    // depth test
    if (Mpe.z() < 0) { continue; }

    // to camera pixels
    int cam_u = static_cast<int>((ufx*Mpe.x())/Mpe.z() + ucx);
    int cam_v = static_cast<int>((ufy*Mpe.y())/Mpe.z() + ucy);

    if ((cam_u < 0 || cam_u >= uw) || (cam_v < 0 || cam_v >= uh)) {
      continue;
    }

    Vec5f uvd_ij;
    uvd_ij[0] = static_cast<float>(cam_u);
    uvd_ij[1] = static_cast<float>(cam_v);
    uvd_ij[2] = static_cast<float>(Mpe.z());
    uvd_ij[3] = static_cast<float>(ij[0]);
    uvd_ij[4] = static_cast<float>(ij[1]);
    uvd_ijs.emplace_back(uvd_ij);
  }

  // with voxsize = 2.0, ~1000 voxels
  // sort from back to front
  std::sort(uvd_ijs.begin(),
            uvd_ijs.end(),
            [](const Vec5f& a, const Vec5f& b) -> bool {
               return a[2] > b[2];
            });

  float voxel_size = ros_wrapper_->get_param<double>("voxel_size");
  for (const Vec5f& uvd_ij : uvd_ijs) {
    int cam_u = static_cast<int>(uvd_ij[0]);
    int cam_v = static_cast<int>(uvd_ij[1]);
    float z = uvd_ij[2];
    int r = static_cast<int>(std::ceil(voxel_size*(ufx/2.)/z));
    // rough splat according to z
    for (int cam_v2 = cam_v - r; cam_v2 < cam_v + r; ++cam_v2) {
      if (cam_v2 <= 0 || cam_v2 >= uh) { continue; }
      for (int cam_u2 = cam_u - r; cam_u2 < cam_u + r; ++cam_u2) {
        if (cam_u2 <= 0 || cam_u2 >= uw) { continue; }
        depth.at<float>(cam_v2, cam_u2) = z;
        ijmap.at<cv::Vec2i>(cam_v2, cam_u2)[0] = (int)uvd_ij[3];
        ijmap.at<cv::Vec2i>(cam_v2, cam_u2)[1] = (int)uvd_ij[4];
      }
    }
  }
}/*}}}*/

void HeightReprojection::raysurf_map(const Isometry3f& M,/*{{{*/
                                     const CamInfo& caminfo,
                                     HeightGrid::Ptr grid,
                                     const ros::Time& ts,
                                     cv::Mat& depth,
                                     cv::Mat& ijmap) {
  CA_AUTO_TICTOC(raysurf_map);

  // render the grid onto depth (which also acts as z-buffer)
  // and ijmap (which maps for each pixel to an ijk voxel).
  // then semantics uses ijmap to project semantics to map.
  float ufx = caminfo.fx;
  float ufy = caminfo.fy;
  float invfx = 1./ufx;
  //float invfy = 1./ufy;
  float ucx = caminfo.cx;
  float ucy = caminfo.cy;

  //ROS_INFO_STREAM("ufx = " << ufx << " ufy = " << ufy << " ucx " << ucx << " ucy " << ucy);

  Isometry3f Minv(M.inverse());
  Vec3f origin_w = M.translation();

  Vec2Ix origin_ij(grid->world_to_grid(origin_w.head<2>()));

  HeightGrid::BoxType grid_bbox(grid->bbox());

  BresenhamIterator2 bitr;

  // wave surfing algorithm
  // - for each pixel u, raysurf along height map (in 2D) w/ bresenham,
  //   from min depth up to max depth
  // - maintain height buffer (same size as image)
  // - project each (x, y, height) seen to camera u, v
  // - if the v is more than height, voxel is not visible since it is below camera FOV.
  //   continue.
  // - if the v is less than 0, then voxel is not visible since it is above
  //   camera FOV. break out of this ray, since ray only goes up.
  // - if lowest pixel v (highest in image) at u is more than current
  //   then ignore, because previously seen height occludes current height
  // - else, "fill in" current height downwards in the image until we hit
  //   another valid height value in the height buffer. here we could
  //   interpolate between the current and previous height for smoother result,
  //   but we currently don't. Note that this assumes camera is upright.
  // - another solution: simply ignore frames if roll is above threshold.
  //   TODO
  //   - account for roll (draw diagonal lines instead of columns).
  //     seems to me easiest option is create virtual upright camera,
  //     do surfing in this space, then rotate back in image space.
  //   - possibly optimize for larger steps at larger distances
  //   - adaptive step size, binary-search type thing?

  // note low numerically is higher in image
  // we initialize with height
  std::vector<int> lowest_v_pixel(caminfo.width, caminfo.height);

  HeightGrid::Accessor acc(grid->get_accesor());

  //int pxcnt = 0;

  float reproj_min_z = ros_wrapper_->get_param<double>("reproj_min_z");
  float reproj_max_z = ros_wrapper_->get_param<double>("reproj_max_z");

  for (int u=0; u < caminfo.width; ++u) {
    {
      float u_ct = (static_cast<float>(u) - ucx)*invfx;
      // direction in camera space
      Vec3f ray_dir_c(u_ct, 0.f, 1.f);
      ray_dir_c.normalize();

      // direction in world space
      Vec3f ray_dir_w(M.linear() * ray_dir_c);

      Vec3f ray_start_w(origin_w + reproj_min_z*ray_dir_w);
      Vec3f ray_end_w(origin_w + reproj_max_z*ray_dir_w);

      Vec2Ix ray_start_ij(grid->world_to_grid(ray_start_w.head<2>()));
      Vec2Ix ray_end_ij(grid->world_to_grid(ray_end_w.head<2>()));

      // initialize 2D bresenham
      bitr.reset(ray_start_ij[0],
                 ray_start_ij[1],
                 ray_end_ij[0],
                 ray_end_ij[1]);
    }

    float last_height = std::numeric_limits<float>::quiet_NaN();

    for (; !bitr.done(); bitr.step()) {
      Vec2Ix ray_ij(bitr.i(), bitr.j());

      // if we get out of grid bounding box, bail
      // TODO maybe clip world ray against box?
      // TODO avoiding this now because of extrapolation
      //if (!grid_bbox.contains(ray_ij)) { continue; }

      // ray end in world (only 2D)
      Vec2f ray_w(grid->grid_to_world(ray_ij));

      // now extrapolating in this case
      //if (!acc.probe_value(ray_ij, height)) { continue; }

      // try to extrapolate
      float height;
      {
        HeightCell hcell;
        if (!acc.probe_value(ray_ij, hcell)) {
          if (std::isnan(last_height)) {
            continue; // give up
          }
          // naive extrapolation
          hcell.height = last_height;
          hcell.extrapolated = true;
          acc.set_value(ray_ij, hcell);
          height = last_height;
        } else {
          // TODO consider extrapolated cells differently?
          if (hcell.extrapolated && std::isfinite(last_height)) {
            hcell.height = last_height;
            acc.set_value(ray_ij, hcell);
          }
          height = hcell.height;
          last_height = height;
        }
      }

      // ray end in world (3D)
      //Vec3f xyz_w(ray_w.x(), ray_w.y(), cell.mheight);
      Vec3f xyz_w(ray_w.x(), ray_w.y(), height);

      // project back to cam, get v pixel
      Vec3f xyz_c = Minv*xyz_w;

      int v = static_cast<int>((ufy*xyz_c.y())/xyz_c.z() + ucy);
      //int v = static_cast<int>(yz_to_pixv(xyz_c.y(), xyz_c.z(), ufy, ucy));

      // remember that in uv coords v goes down as it increases
      // we went above camera vfov, break
      if (v < 0) { break; }

      // voxels are below camera (not visible)
      if (v >= caminfo.height) { continue; }

      //pxcnt += 1;

      // TODO equality case
      if (v > lowest_v_pixel[u]) {
        // cell is occluded by previously drawn pixels
        continue;
      } else {
        // draw span
        for (int v2 = v; v2 < lowest_v_pixel[u]; ++v2) {
          depth.at<float>(v2, u) = xyz_c.z();
          ijmap.at<cv::Vec2i>(v2, u) = cv::Vec2i(ray_ij[0], ray_ij[1]);
        }
        // update lowest (highest in image) pixel
        lowest_v_pixel[u] = v;
      }
    }
  }

  //ROS_INFO_STREAM("ijmap size " << ijmap.cols << ", " << ijmap.rows);
  //ROS_INFO_STREAM("pxcnt = " << pxcnt);

  if (ros_wrapper_->get_param<bool>("publish_reproj_depth")) {
    CA_AUTO_TICTOC(publish_reproj_depth);
    if (ros_wrapper_->has_subscribers("reproj_depth")) {
      ros_wrapper_->publish_cv("reproj_depth", depth, "32FC1", ts, COLOR_FRAME_ID);
    }
  }

  if (ros_wrapper_->get_param<bool>("publish_reproj_disp")) {
    this->publish_reproj_disp(COLOR_FRAME_ID, ts, depth);
  }

  if (ros_wrapper_->get_param<bool>("publish_reproj_depth_pc")) {
    this->publish_reproj_depth_pc(COLOR_FRAME_ID, ts, depth, caminfo);
  }

}/*}}}*/

}
